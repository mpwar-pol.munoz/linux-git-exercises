#!/bin/bash

#---  Variables  ---#

# Variables holding different parts of paths
STUDENT="pol"                                       # Student name
BASE="backup/$STUDENT"                              # Base for the directories we'll be working with
DIR="+%Y/%m/%d"                                     # Date command format for directories
DATE="+%Y%m%d"                                      # Date command format for log files
DIRECTORY="$BASE/$(date $DIR)"                      # Current day directory
LOGS="nginx_log_requests_"                          # Base for the logs file name
FILE="$DIRECTORY/$LOGS$(date $DATE).log"            # Full file path for today, combining different previous variables

# Variable holding the logs file link
LINK="https://raw.githubusercontent.com/elastic/examples/master/Common%20Data%20Formats/nginx_logs/nginx_logs"
 
# Variable holding the days of the week as flags
DAYS=("-Mon" "-Tue" "-Wed" "-Thu" "-Fri" "-Sat" "-Sun")


#---  Actual code  ---#

# Create the current day directory if it doesn't exists
[ ! -d $DIRECTORY ] && mkdir -p $DIRECTORY

# Download the logs to the required file
curl -o $FILE $LINK

# If the current day of the week is Sunday (7)
if [ $(date +%u) -eq 7 ]; then

    # Path to the .tar file
    TAR="$DIRECTORY/$LOGS$(date $DATE).tar"

    # Variable that will change the tar mode from create in the first iteration to update in the rest
    MODE="c"

    # For each day of the week
    for DAY in ${DAYS[@]}; do

        # Variable storing the path to that day's directory
        OLD=$"$BASE/$(date -v $DAY $DIR)"

        # If that directory exists (just in case) add the corresponding log file to the tar, creating it in the first iteration
        # (Note that the -C flag is used to change to that day's directory, taring only the files themselves)
        [ -d $OLD ] && tar -"$MODE"vf $TAR -C $OLD "$LOGS$(date -v $DAY $DATE).log"

        # Change the mode to update (only useful in the first iteration where the mode was create)
        MODE="u"
    done

    # Compress the result, as updating a tar file isn't possible if it's compressed
    gzip -f $TAR
fi