# Parte I Ejercicio 2 - Script bash

Se ha creado el archivo `backup.sh` que define el proceso de backup a ejecutar. El script está comentado para explicar su enfoque y funcionamiento.

**Nota:** Para poder ejecutarlo hay que asignarle los permisos correctos mediante `chmod` o incluso cambiar su propietario mediante `chown`, según se prefiera.

**Nota Importante:** Se ha usado el directorio `backup/<student_name>/<year>/<month>/<day>` en vez del requerido en el enunciado ya que des de la versión 10.15 del sistema operativo macOS el directorio root es de solo lectura.
