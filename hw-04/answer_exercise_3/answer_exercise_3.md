# Parte I Ejercicio 3 - Cron

Después de realizar el ejercicio anterior, se puede añadir una entrada a la tabla cron del usuario actual usando el comando:

```
crontab -e
```

![Terminal screenshot](imgs/cron_edit.png "Terminal screenshot for editing the crontab")

Para cumplir los requerimientos del enunciado, se ha usado el siguiente schedule:

```
59	23	*	*	*   /path/to/script/backup.sh
```

Que puede ser comprobado mediante:

```
crontab -l
```

![Terminal screenshot](imgs/cron_list.png "Terminal screenshot for listing the crontab")

**Nota:** Por el comportamiento de macOS mencionado anteriormente, se ha usado el path absoluto hasta el script en vez de moverlo a una carpeta más genérica como `/scripts` o `/usr/local/bin`.