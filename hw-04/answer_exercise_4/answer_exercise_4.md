# Parte II - Git

El proyecto se puede encontrar en [este enlace](https://gitlab.com/mpwar-pol.munoz/git-demo). En el orden requerido por el enunciado, se ha seguido el siguiente proceso:

1. Al usar un remoto, hay dos opciones para crear el repositorio local:

    * Desde la carpeta `git-demo`:

    ```
    git init
    ```
    ```
    git remote add origin https://gitlab.com/mpwar-pol.munoz/git-demo
    ```

    * Desde la carpeta padre de `git-demo` (se ha usado esta opción):

    ```
    git clone https://gitlab.com/mpwar-pol.munoz/git-demo
    ```
    ```
    cd git-demo
    ```


2. Las reglas para ignorar archivos se definen en el archivo `.gitignore`, que se puede definir como pide el enunciado de la siguiente forma:

    ```
    echo "*.sh" > .gitignore
    ```

3. Los archivos se pueden crear mediante el comando `touch`, para luego ser añadidos al staging area. Para que el resto del ejercicio tenga más sentido, se ha realizado un commit después de añadirlos.

    ```
    touch demo1.txt demo2.txt
    ```
    ```
    git add demo1.txt demo2.txt
    ```
    ```
    git commit -m "Added demo1.txt and demo2.txt"
    ```

4. Hay que tener en cuenta que hay que volver a añadir los cambios al staging area:

    ```
    echo "Some content" >> demo1.txt
    ```
    ```
    git add demo1.txt
    ```
    ```
    git commit -m "Modified demo1.txt"
    ```

5. De forma parecida al punto anterior:

    ```
    echo "Some content" >> demo2.txt
    ```
    ```
    git add demo2.txt
    ```
    ```
    git commit -m "Modified demo3.txt"
    ```

6. Se puede modificar el mensaje del último commit mediante:

    ```
    git commit --amend -m "Modified demo2.txt"
    ```

7. Para crear la rama `develop` y cambiar a ella en un mismo comando se puede usar:

    ```
    git checkout -b develop
    ```
    El archivo se puede crear de la siguiente forma:
    ```
    echo "#\!/bin/bash\necho \"Git 101\"" > script.sh
    ```

8. Se pueden dar permisos de ejecución al usuario propietario del archivo mediante:

    ```
    chmod 700 script.sh
    ```
    Y de paso comprobar su funcionamiento mediante:
    ```
    ./script.sh
    ```
    Para añadir el archivo creado al repositorio se realiza el mismo proceso que anteriormente, aunque forzando el `add` por los contenidos de nuestro .gitignore.
    ```
    git add -f script.sh
    ```
    ```
    git commit -m "Added script.sh"
    ```

9. Para hacer el merge primero tenemos que ir a la rama master:

    ```
    git checkout master
    ```
    ```
    git merge develop
    ```

10. Finalmente, se puede realizar el push:

    ```
    git push -u origin master
    ```


Finalmente, y como se pedía en el enunciado, se ha obtenido un archivo de logs usando la instrucción:

```
git log --oneline > verification.log
```

También se puede ver el proceso más en detalle en la siguiente captura:

![Terminal screenshot](imgs/git.png "Terminal screenshot for the git process")

Y el resultado de obtener los logs en la siguiente:

![Terminal screenshot](imgs/git_logs.png "Terminal screenshot for the git logs")
