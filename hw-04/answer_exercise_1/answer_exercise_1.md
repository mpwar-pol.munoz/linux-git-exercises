# Parte I Ejercicio 1 - Procesando logs

El primer paso, descrito en el enunciado, consiste en descargar los logs a procesar mediante el comando: 

```
curl -o nginx_logs_examples.log https://raw.githubusercontent.com/elastic/examples/master/Common%20Data%20Formats/nginx_logs/nginx_logs 
```

![Terminal screenshot](imgs/logs_curl.png "Terminal screenshot for downloading the logs")

A continuación, se debe ejecutar lo siguiente para generar el archivo deseado:

```
cut -d ' ' -f 1 nginx_logs_examples.log | sort | uniq -c | awk '{ print $2" -> "$1 }' > nginx_requests_total.txt
```

**Explicación:** La respuesta está formada por las siguientes partes:

* `cut -d ' ' -f 1 nginx_logs_examples.log`: Separa las líneas del archivo `nginx_logs_examples.log` usando un espacio como delimitador y muestra el primer campo de cada una. En este ejercicio, mostraría la IP de cada línea del log.

![Terminal screenshot](imgs/logs_cut.png "Terminal screenshot for cutting the logs")

* `| sort`: Recibe las líneas del comando anterior (gracias al operador `|`) y las ordena.

![Terminal screenshot](imgs/logs_sort.png "Terminal screenshot for sorting the cut logs")

* `| uniq -c`: Cuenta las repeticiones de cada línea única en el comando anterior y muestra tanto el valor como el número de veces que aparece.

![Terminal screenshot](imgs/logs_uniq.png "Terminal screenshot for finding unique logs")

* `| awk '{ print $2" -> "$1 }'`: Formatea el resultado del comando anterior de la forma que pide el enunciado. El funcionamiento concreto es parecido al del primer comando del ejercicio, pero permite ejecutar operaciones sobre el resultado, incluyendo `print` para formatear los campos con más libertad.

![Terminal screenshot](imgs/logs_awk.png "Terminal screenshot for formatting logs")

* `> nginx_requests_total.txt`: Redirige la salida del comando anterior al archivo `nginx_requests_total.txt`.


**Nota:** Como se puede observar, el resultado de cada apartado se ha limitado a las primeras 10 líneas mediante el comando `head` para realizar las capturas de pantalla.